package com.bsa.chat.demo.controller;


import com.bsa.chat.demo.dto.AuthUserDTO;
import com.bsa.chat.demo.dto.UserLoginDTO;
import com.bsa.chat.demo.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
public class AuthController {
    @Autowired
    private AuthService authService;


    @PostMapping("/login")
    public AuthUserDTO login(@RequestBody UserLoginDTO user) throws Exception {
        return authService.login(user);
    }
}

