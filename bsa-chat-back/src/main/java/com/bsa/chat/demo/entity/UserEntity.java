package com.bsa.chat.demo.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class UserEntity {

    @Id
    @GeneratedValue
    UUID id;


    @OneToMany(fetch = FetchType.LAZY)
    List<MessageEntity> messageEntities;

    @Basic
    String firstName;

    @Basic
    String lastName;

    @Basic
    String email;

    @Basic
    String password;

    @Basic
    String avatar;

    @OneToMany
    List<MessageEntity> likedMessages;
}
