package com.bsa.chat.demo.controller;

import com.bsa.chat.demo.dto.GetMessageDto;
import com.bsa.chat.demo.dto.PostMessageDto;
import com.bsa.chat.demo.service.MessageService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/messages")
public class MessageController {

    final MessageService messageService;

    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @GetMapping(value = "/all")
    public List<GetMessageDto> getAllMessages() {
        return this.messageService.getAllMessages();
    }

    @GetMapping(value = "/{id}")
    public GetMessageDto getMessageById(@PathVariable String id){
        return messageService.getMessageById(id);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(value = "/save")
    public void saveMessage(@RequestBody PostMessageDto postMessageDto) {
        this.messageService.saveMessage(postMessageDto);
    }

    @PutMapping(value = "/edit/{id}")
    public void editMessage(@PathVariable String id, @RequestParam String editedAt, @RequestParam String text) {
        System.out.println(text);
        messageService.setEdited(id, editedAt, text);
    }

    @DeleteMapping
    public void deleteMessage(@RequestParam String id) {
        messageService.deleteMessage(id);
    }

}
