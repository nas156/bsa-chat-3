package com.bsa.chat.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class PostMessageDto {
    String id;
    UUID userId;
    String text;
    String createdAt;
}
