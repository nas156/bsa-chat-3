package com.bsa.chat.demo;

import com.bsa.chat.demo.entity.MessageEntity;
import com.bsa.chat.demo.entity.UserEntity;
import com.bsa.chat.demo.repository.MessageRepository;
import com.bsa.chat.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
public class ChatApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChatApplication.class, args);
    }


    @Bean
    CommandLineRunner initDatabase(MessageRepository messageRepository, UserRepository userRepository, PasswordEncoder passwordEncoder) {
        var user1 = UserEntity.builder()
                .avatar("https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA")
                .lastName("Man")
                .firstName("Den")
                .email("aaa@aa.com")
                .password(passwordEncoder.encode("111"))
                .build();
        var user2 = UserEntity.builder()
                .avatar("https://resizing.flixster.com/EVAkglctn7E9B0hVKJrueplabuQ=/220x196/v1.cjs0NjYwNjtqOzE4NDk1OzEyMDA7MjIwOzE5Ng")
                .lastName("Ken")
                .firstName("Wen")
                .email("aba@aa.com")
                .password(passwordEncoder.encode("121"))
                .build();
        var user3 = UserEntity.builder()
                .avatar("https://resizing.flixster.com/EVAkglctn7E9B0hVKJrueplabuQ=/220x196/v1.cjs0NjYwNjtqOzE4NDk1OzEyMDA7MjIwOzE5Ng")
                .lastName("Ken")
                .firstName("admin")
                .email("admin@admin.com")
                .password(passwordEncoder.encode("admin"))
                .build();
        var message1 = MessageEntity.builder().createdAt("2020-07-16T19:48:42.481Z").text("aaa").user(user1).editedAt("").id("1ksf").build();
        var message2 = MessageEntity.builder().createdAt("2020-07-16T19:49:44.481Z").text("aba").user(user2).editedAt("").id("2fsadf").build();
        var message3 = MessageEntity.builder().createdAt("2020-07-16T19:49:45.481Z").text("ala").user(user2).editedAt("").id("2fsadk").build();
        return args -> {
            userRepository.save(user3);
            userRepository.save(user1);
            userRepository.save(user2);
            messageRepository.save(message1);
            messageRepository.save(message2);
            messageRepository.save(message3);
        };
    }
}
