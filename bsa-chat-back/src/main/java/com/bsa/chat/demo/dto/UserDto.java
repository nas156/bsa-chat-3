package com.bsa.chat.demo.dto;

import lombok.*;

import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserDto {
    UUID id;
    String firstName;
    String lastName;
    String email;
    String password;
    String avatar;
}
