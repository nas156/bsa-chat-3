package com.bsa.chat.demo.service;

import com.bsa.chat.demo.dto.GetMessageDto;
import com.bsa.chat.demo.dto.PostMessageDto;
import com.bsa.chat.demo.entity.MessageEntity;
import com.bsa.chat.demo.repository.MessageRepository;
import com.bsa.chat.demo.repository.UserRepository;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MessageService {

    final MessageRepository messageRepository;
    final UserRepository userRepository;

    public MessageService(MessageRepository messageRepository, UserRepository userRepository) {
        this.messageRepository = messageRepository;
        this.userRepository = userRepository;
    }

    public List<GetMessageDto> getAllMessages(){
        return messageRepository.findAll().stream()
                .sorted(Comparator.comparing(e -> new DateTime(e.getCreatedAt()))).map(x -> GetMessageDto.builder()
                                            .avatar(x.getUser().getAvatar())
                                            .text(x.getText())
                                            .editedAt(x.getEditedAt())
                                            .createdAt(x.getCreatedAt())
                                            .user(x.getUser().getFirstName())
                                            .id(x.getId())
                                            .userId(x.getUser().getId())
                                            .build()).collect(Collectors.toList());
    }


    public void saveMessage(PostMessageDto postMessageDto) {
        this.messageRepository.save(MessageEntity.builder()
                .createdAt(postMessageDto.getCreatedAt())
                .editedAt("")
                .id(postMessageDto.getId())
                .text(postMessageDto.getText())
                .user(userRepository.findById(postMessageDto.getUserId()).get())
                .build());
    }

    public void setEdited(String messageId, String editedAt, String text){
        var message = messageRepository.findById(messageId).get();
        message.setText(text);
        message.setEditedAt(editedAt);
        messageRepository.save(message);
    }

    public void deleteMessage(String id) {
        messageRepository.deleteById(id);
    }

    public GetMessageDto getMessageById(String id) {
        return messageRepository.findById(id).map(x->GetMessageDto.builder()
                .avatar(x.getUser().getAvatar())
                .createdAt(x.getCreatedAt())
                .editedAt(x.getEditedAt())
                .id(x.getId())
                .text(x.getText())
                .user(x.getUser().getFirstName())
                .userId(x.getUser().getId())
                .build()).get();
    }
}
