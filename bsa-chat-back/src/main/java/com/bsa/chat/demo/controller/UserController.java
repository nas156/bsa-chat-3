package com.bsa.chat.demo.controller;

import com.bsa.chat.demo.dto.UserDto;
import com.bsa.chat.demo.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@CrossOrigin
@RequestMapping("/users")
@RestController
public class UserController {

    final UserService userService;


    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/all")
    public List<UserDto> getAllUsers() {
        return userService.getAllUsers();
    }

    @GetMapping("/{id}")
    public UserDto getUser(@PathVariable UUID id) {
        return userService.getUser(id);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/add")
    public void addUser(@RequestBody UserDto userDto) {
        userService.addUser(userDto);
    }

    @ResponseStatus(HttpStatus.ACCEPTED)
    @PutMapping("/update/{id}")
    public void updateUser(@RequestBody UserDto userDto, @PathVariable UUID id) {
        userService.updateUser(userDto, id);
    }

    @PutMapping("{id}/like")
    public void likeMessage(@PathVariable UUID id, @RequestBody List<String> messages){
        userService.likeMessage(id, messages);
    }

    @GetMapping("{id}/like")
    public List<String> getLikedMessages(@PathVariable UUID id){
        return userService.getLikedMessages(id);
    }

    @ResponseStatus(HttpStatus.ACCEPTED)
    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable UUID id) {
        userService.deleteUser(id);
    }

}
