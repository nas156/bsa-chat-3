package com.bsa.chat.demo.service;

import com.bsa.chat.demo.dto.UserDto;
import com.bsa.chat.demo.entity.AuthUser;
import com.bsa.chat.demo.entity.MessageEntity;
import com.bsa.chat.demo.entity.UserEntity;
import com.bsa.chat.demo.repository.MessageRepository;
import com.bsa.chat.demo.repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UserService implements UserDetailsService {
    final UserRepository userRepository;
    final MessageRepository messageRepository;
    final PasswordEncoder passwordEncoder;

    public UserService(UserRepository userRepository, MessageRepository messageRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.messageRepository = messageRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public AuthUser loadUserByUsername(String email) throws UsernameNotFoundException {
        return userRepository
                .findByEmail(email)
                .map(user -> new AuthUser(user.getId(), user.getEmail(), user.getPassword()))
                .orElseThrow(() -> new UsernameNotFoundException(email));
    }

    public List<UserDto> getAllUsers() {
        return userRepository.findAll().stream().map(x -> UserDto.builder()
                .id(x.getId())
                .avatar(x.getAvatar())
                .email(x.getEmail())
                .lastName(x.getLastName())
                .firstName(x.getFirstName())
                .password(x.getPassword())
                .build()).collect(Collectors.toList());
    }

    public void addUser(UserDto userDto) {
        userRepository.save(UserEntity.builder()
                .password(passwordEncoder.encode(userDto.getPassword()))
                .email(userDto.getEmail())
                .firstName(userDto.getFirstName())
                .lastName(userDto.getLastName())
                .avatar(userDto.getAvatar())
                .build());
    }

    public void updateUser(UserDto userDto, UUID id) {
        var user = userRepository.findById(id).get();
        user.setAvatar(userDto.getAvatar());
        user.setEmail(userDto.getEmail());
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        if (!userDto.getPassword().isBlank()) {
            user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        }
        userRepository.save(user);
    }

    public void deleteUser(UUID id) {
        messageRepository.findAllByUser_Id(id).forEach(messageRepository::delete);
        userRepository.deleteById(id);
    }

    public UserDto getUser(UUID id) {
        return userRepository.findById(id).map(x -> UserDto.builder()
                .id(x.getId())
                .password("")
                .firstName(x.getFirstName())
                .email(x.getEmail())
                .avatar(x.getAvatar())
                .lastName(x.getLastName())
                .build()).get();
    }

    public void likeMessage(UUID id, List<String> messages) {
        var user = userRepository.findById(id).get();
        user.setLikedMessages(messages.stream().map(x -> messageRepository.findById(x).get()).collect(Collectors.toList()));
        userRepository.save(user);
    }

    public List<String> getLikedMessages(UUID id) {
        var user = userRepository.findById(id).get();
        return user.getLikedMessages().stream().map(MessageEntity::getId).collect(Collectors.toList());
    }

    public UserDto getUserById(UUID id) {
        return userRepository.findById(id).map(x -> UserDto.builder()
                .lastName(x.getLastName())
                .avatar(x.getAvatar())
                .email(x.getEmail())
                .firstName(x.getFirstName())
                .password(x.getPassword())
                .id(x.getId())
                .build()).get();
    }
}
