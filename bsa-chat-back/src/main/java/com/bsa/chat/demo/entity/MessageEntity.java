package com.bsa.chat.demo.entity;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class MessageEntity {

    @Id
    String id;

    @ManyToOne(fetch = FetchType.LAZY)
    UserEntity user;

    @Basic
    String createdAt;

    @Basic
    String editedAt;

    @Basic
    String text;

}
