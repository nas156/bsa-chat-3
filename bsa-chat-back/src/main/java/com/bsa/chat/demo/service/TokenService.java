package com.bsa.chat.demo.service;

import com.bsa.chat.demo.entity.AuthUser;
import com.bsa.chat.demo.repository.UserRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKey;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;

import static com.bsa.chat.demo.config.SecurityConstants.EXPIRATION_TIME;

@Service
public class TokenService {

    @Autowired
    UserRepository userRepository;

    private final String SECRET_KEY="eyJhbGciOiJIUzI1NiJ9eyJzdWIiOiJKb2UifQ1KP0SsvENi7Uz1oQc07aXTL7kpQG5jBNIybqr601234561dds";

    public String extractUserid(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    public Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }

    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }
    private Claims extractAllClaims(String token) {
        return Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token).getBody();
    }

    public Boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }

    public String generateToken(AuthUser userDetails) {
        Map<String, Object> claims = new HashMap<>();
        return createToken(claims, userDetails.getId());
    }

    private String createToken(Map<String, Object> claims, UUID subject) {
//        SecretKey key = Keys.secretKeyFor(new SignatureAlgorithm(SECRET_KEY));
//        return Jwts.builder()
//                .setClaims(claims)
//                .setSubject(subject.toString())
//                .setIssuedAt(new Date(System.currentTimeMillis()))
//                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
//                .signWith(SignatureAlgorithm.HS256, SECRET_KEY)
//                .compact();
        return userRepository.findById(subject).get().getEmail();
    }

    static public UUID getUserId(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        var currentUserId = (String)auth.getPrincipal();
        return  UUID.fromString(currentUserId);
    }
}
