import { call, put, takeEvery, all } from "redux-saga/effects";
import axios from 'axios';
import { FETCH_MESSAGE, FETCH_MESSAGE_SUCCESS } from "./editActionsTypes";

export function* fetchMessage(action) {
    const { id } = action.payload;
    const token = localStorage.getItem('token');
    const headerParams = { "Authorization": `Bearer ${token}` }
    try {
        const message = yield call(axios.get, `http://localhost:8080/messages/${id}`, {headers: headerParams});
        yield put({ type: FETCH_MESSAGE_SUCCESS, payload: { message: message.data.text, id: message.data.id } });
    }catch(error){
        console.log(error.message);
    }
}

function* watchFetchMesaage() {
    yield takeEvery(FETCH_MESSAGE, fetchMessage);
}

export default function* editSagas() {
    yield all([
        watchFetchMesaage()
    ]);
}