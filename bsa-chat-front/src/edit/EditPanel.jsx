import React from 'react';
import * as actions from './editActions';
import { connect } from 'react-redux';
import { editMessage } from '../chat/chatActions';

class EditPanel extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            message: '',
            id: ''
        };
        this.onChange = this.onChange.bind(this);
        this.onCancel = this.onCancel.bind(this);
        this.onApplyEdit = this.onApplyEdit.bind(this);
    }

    static getDerivedStateFromProps(props, state) {
        if (props.id !== state.id && props.id !== '') {
            return ({
                message: props.message,
                id: props.id
            });
        } else { 
            return null;
        }
    }

    onChange(e) {
        const value = e.target.value;
        this.setState({
            message: value
        });
    }

    onApplyEdit(date) {
        this.props.editMessage(this.state.message, this.props.id, date);
        this.props.history.push("/chat");
        this.setState({
            message: '',
            id: ''
        });
        this.props.dropMessageTextAndId();
    }

    onCancel() {
        this.setState({
            message: '',
            id:''
        });
        this.props.history.push("/chat");
        this.props.dropMessageTextAndId();
    }

    render() {
        return (
            <div className='edit-panel'>
                <span>Edit message</span>
                <textarea value = {this.state.message} placeholder="Edit message"
                    onChange={(e) => this.onChange(e)}></textarea>
                <div>
                    <button className="edit-button" onClick={() => this.onApplyEdit(new Date().toISOString())}>edit
                        </button>
                    <button className="cancel" onClick={() => this.onCancel()}>cancel</button>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        message: state.edit.message,
        id: state.edit.id
    }
}

const mapDispatchToProps = {
    ...actions,
    editMessage
}

export default connect(mapStateToProps, mapDispatchToProps)(EditPanel);
