import {
    CANCEL,
    DROP_CURRENT_MESSAGE_TEXT_ID,
    FETCH_MESSAGE
} from "./editActionsTypes";

export const cancel = () => ({
    type: CANCEL
});


export const dropMessageTextAndId = () => ({
    type: DROP_CURRENT_MESSAGE_TEXT_ID
});


export const fetchMessage = (id) => ({
    type: FETCH_MESSAGE,
    payload: {
        id
    }
});