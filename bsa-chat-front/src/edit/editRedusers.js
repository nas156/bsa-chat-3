import { DROP_CURRENT_MESSAGE_TEXT_ID, FETCH_MESSAGE_SUCCESS } from "./editActionsTypes";

const ititialState = {
    id: '',
    message: ''
};

export default function (state = ititialState, action) {
    switch (action.type) {
        case FETCH_MESSAGE_SUCCESS: {
            const { message, id } = action.payload;
            return ({
                message: message,
                id: id
            })
        }

        case DROP_CURRENT_MESSAGE_TEXT_ID: {
            return ({
                message: '',
                id: ''
            });
        }

        default:
            return state;
    }

}
