import React from 'react';
import './Chat.css';
import logo from './logo.jpg'
import Header from './common/Header';
import ChatPanel from './chat/components/ChatPanel';
import Footer from './common/Footer';
import EditPanel from './edit/EditPanel';
import { Route, Switch } from 'react-router';
import Users from './users/Users';
import EditUsers from './editUsers/EditUsers';
import LoginForm from './login/LoginForm';



class Chat extends React.Component {

    render() {
        return (
                < div className="chat">
                    <Header imageSrc={logo} />
                    <Switch>
                        <Route exact path="/" component={LoginForm}/>
                        <Route exact path="/chat" component={ChatPanel} />
                        <Route exact path="/edit" component={EditPanel} />
                        <Route exact path="/users" component={Users} />
                        <Route exact path="/editUsers" component={EditUsers} />
                    </Switch>
                    <Footer />
                </div>
        );
    }
}

export default Chat;
