export const FETCH_USER = "FETCH_USER";
export const FETCH_USER_SUCCESS = "FETCH_USER_SUCCESS";
export const ADD_USER = "ADD_USER";
export const EDIT_USER = "EDIT_USER";
export const DROP_EDITING_DATA = "DROP_EDITING_DATA";