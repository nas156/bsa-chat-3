import { FETCH_USER, EDIT_USER, ADD_USER, DROP_EDITING_DATA } from "./editUsersActionTypes";

export const fetchUser = (id) => ({
    type: FETCH_USER,
    payload: {
        id
    }
});

export const addUser = (userData) => ({
    type: ADD_USER,
    payload: {
        userData
    }
});

export const editUser = (userData) => ({
    type: EDIT_USER,
    payload: {
        userData
    }
});

export const dropEditingData = () => ({
    type: DROP_EDITING_DATA
});