import React from 'react';
import { getDefaultUserData } from '../helper/userHelper';
import * as actions from './editUsersActions';
import { connect } from 'react-redux';

class EditUsers extends React.Component {
    constructor(props) {
        super(props);
        this.state = getDefaultUserData();
    }


    static getDerivedStateFromProps(props, state) {
        if (props.userData.id !== state.id) {
            return ({
                ...props.userData
            });
        } else {
            return null;
        }
    }

    onChange(e, name) {
        const value = e.target.value;
        this.setState({
            ...this.state,
            [name]: value
        });
    }

    send() {
        if (this.state.id) {
            this.props.editUser(this.state);
        } else {
            this.props.addUser(this.state);
        }
        this.setState({
            ...getDefaultUserData()
        })
        this.props.dropEditingData();
        this.props.history.push("/users");
    }

    cancel() {
        this.setState({
            ...getDefaultUserData()
        })
        this.props.dropEditingData();
        this.props.history.push("/users");
    }

    render() {
        if(this.props.user!=='admin'){
            return <div className="loader"></div>
        }
        return (
            <div className="edit-users">
                <label>fist name</label>
                <input value={this.state.firstName} type="text" onChange={(e) => this.onChange(e, 'firstName')} />
                <label>last name</label>
                <input value={this.state.lastName} type="text" onChange={(e) => this.onChange(e, 'lastName')} />
                <label>email</label>
                <input value={this.state.email} type="email" onChange={(e) => this.onChange(e, 'email')} />
                <label>avatar</label>
                <input value={this.state.avatar} type="text" onChange={(e) => this.onChange(e, 'avatar')} />
                <label>new password</label>
                <input value={this.state.password} type="password" onChange={(e) => this.onChange(e, 'password')} />
                <button onClick={() => this.send()}>send</button>
                <button onClick={() => this.cancel()}>cancel</button>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        userData: state.editUsers.userData,
        user: state.chat.user
    }
}

const mapDispatchToProps = {
    ...actions
}

export default connect(mapStateToProps, mapDispatchToProps)(EditUsers);