import { call, put, takeEvery, all } from "redux-saga/effects";
import axios from "axios";
import { FETCH_USER_SUCCESS, FETCH_USER, EDIT_USER, ADD_USER } from "./editUsersActionTypes";
import { FETCH_USERS } from "../users/usersActionTypes";

export function* getUserData(action) {
    const { id } = action.payload;
    const token = localStorage.getItem('token');
    const headerParams = { "Authorization": `Bearer ${token}` }
    try {
        const user = yield call(axios.get, `http://localhost:8080/users/${id}`, {headers: headerParams});
        yield put({ type: FETCH_USER_SUCCESS, payload: { userData: user.data } });
    } catch (error) {
        console.log(error);
    }
}

function* watchGetUserData() {
    yield takeEvery(FETCH_USER, getUserData);
}

export function* editUser(action) {
    const { userData } = action.payload;
    const id = userData.id;
    const token = localStorage.getItem('token');
    const headerParams = { "Authorization": `Bearer ${token}` }
    try {
        yield call(axios.put, `http://localhost:8080/users/update/${id}`, userData, {headers: headerParams});
        yield put({ type: FETCH_USERS });
    } catch (error) {
        console.log(error.message);
    }
}

function* watchEditUser() {
    yield takeEvery(EDIT_USER, editUser);
}

export function* addUser(action) {
    const { userData } = action.payload;
    const token = localStorage.getItem('token');
    const headerParams = { "Authorization": `Bearer ${token}` }
    try {
        yield call(axios.post, `http://localhost:8080/users/add`, userData, {headers: headerParams});
        yield put({ type: FETCH_USERS });
    } catch (error) {
        console.log(error.message);
    }
}

function* watchAddUser() {
    yield takeEvery(ADD_USER, addUser);
}

export default function* editUsersSagas() {
    yield all([
        watchGetUserData(),
        watchEditUser(),
        watchAddUser()
    ]);
}