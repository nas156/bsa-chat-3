import { FETCH_USER_SUCCESS, DROP_EDITING_DATA } from "./editUsersActionTypes";

const { getDefaultUserData } = require("../helper/userHelper");

const intialState = {
    userData: getDefaultUserData()
}

export default function (state = intialState, action) {
    switch (action.type) {
        case FETCH_USER_SUCCESS: {
            const { userData } = action.payload;
            return ({
                userData: userData
            });
        }

        case DROP_EDITING_DATA: {
            return({
                userData: getDefaultUserData()
            })
        }

        default:
            return state;
    }
}