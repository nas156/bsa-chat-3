import { FETCH_MESSAGES_SUCCESS, FETCH_LIKED_MESSAGES_SUCCESS, LOGIN_SUCCESS, LOGOUT } from "./chatActionTypes";

const ititialState = {
    userId: localStorage.getItem('userId'),
    user: localStorage.getItem('user'),
    avatar: localStorage.getItem('avatar'),
    isLogined: false,
    list: [],
    users: '10',
    likedMessages: [],
    isFetching: true
};

export default function (state = ititialState, action) {
    switch (action.type) {
        case FETCH_MESSAGES_SUCCESS: {
            const list = action.payload.messages;
            let date = new Date().toISOString();
            if (list.length > 0) {
                date = list[list.length - 1].createdAt;
            }
            return ({
                ...state,
                isFetching: false,
                list: list,
                currentDate: date
            });
        }

        case FETCH_LIKED_MESSAGES_SUCCESS: {
            const { likedMessages } = action.payload;
            return ({
                ...state,
                likedMessages: likedMessages
            });

        }

        case LOGIN_SUCCESS: {
            const { userId, user, avatar, token} = action.payload;
            localStorage.setItem('token', token);
            localStorage.setItem('userId', userId);
            localStorage.setItem('avatar', avatar);
            localStorage.setItem('user', user);
            return({
                ...state,
                isLogined: true,
                userId: userId,
                user: user,
                avatar: avatar
            });
        }

        case LOGOUT:{
            localStorage.setItem('token', '');
            localStorage.setItem('userId', '');
            localStorage.setItem('avatar', '');
            localStorage.setItem('user', '');
            return({
                ...state,
                isLogined: false,
                userId: '',
                user: '',
                avatar: ''
            });
        }

        default:
            return state;
    }

}
