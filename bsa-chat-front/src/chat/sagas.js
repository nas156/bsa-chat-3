import { call, put, takeEvery, all } from "redux-saga/effects";
import axios from 'axios';
import { FETCH_MESSAGES, FETCH_MESSAGES_SUCCESS, ADD_MESSAGE, EDIT_MESSAGE, DELETE_MESSAGE, FETCH_LIKED_MESSAGES, LIKE_MESSAGE, FETCH_LIKED_MESSAGES_SUCCESS, LOGIN_SUCCESS, LOGIN } from "./chatActionTypes";
import { generateId } from "../helper/IdGenerator";



export function* fetchMessages() {
    const token = localStorage.getItem('token');
    const headerParams = { "Authorization": `Bearer ${token}` }

    try {
        const messages = yield call(axios.get, 'http://localhost:8080/messages/all', { headers: headerParams });
        yield put({ type: FETCH_MESSAGES_SUCCESS, payload: { messages: messages.data } })
    }
    catch (error) {
        console.log(error.message)
    }
}

function* watchFetchMessages() {
    yield takeEvery(FETCH_MESSAGES, fetchMessages);
}

export function* addMessage(action) {
    const newMessage = {
        "id": generateId(),
        "userId": action.payload.userId,
        "text": action.payload.text,
        "createdAt": action.payload.date
    }
    const token = localStorage.getItem('token');
    const headerParams = { "Authorization": `Bearer ${token}` }
    try {
        yield call(axios.post, 'http://localhost:8080/messages/save', newMessage, { headers: headerParams });
        yield put({ type: FETCH_MESSAGES });
    }
    catch (error) {
        console.log(error.message);
    }
}

function* watchAddMessage() {
    yield takeEvery(ADD_MESSAGE, addMessage)
}

export function* editMessage(action) {
    const { text, id, date } = action.payload;
    console.log(text)
    try {
        yield call(axios.put, `http://localhost:8080/messages/edit/${id}?editedAt=${date}&text=${text}`);
        yield put({ type: FETCH_MESSAGES });
    } catch (error) {
        console.log(error.message);
    }
}

function* watchEditMessage() {
    yield takeEvery(EDIT_MESSAGE, editMessage);
}

export function* deleteMessage(action) {
    const { id } = action.payload;
    const token = localStorage.getItem('token');
    const headerParams = { "Authorization": `Bearer ${token}` }

    try {
        yield call(axios.delete, `http://localhost:8080/messages?id=${id}`, { headers: headerParams })
        yield put({ type: FETCH_MESSAGES });
    } catch (error) {
        console.log(error.message);
    }
}

function* watchDeleteMessage() {
    yield takeEvery(DELETE_MESSAGE, deleteMessage);
}

export function* likeMessage(action) {
    const token = localStorage.getItem('token');
    const headerParams = { "Authorization": `Bearer ${token}` }
    const { id, likedMessages } = action.payload;
    try {
        yield call(axios.put, `http://localhost:8080/users/${id}/like`, likedMessages, { headers: headerParams });
        yield put({ type: FETCH_LIKED_MESSAGES, payload: { id: id } });
    } catch (error) {
        console.log(error);
    }
}

function* watchLikeMessage() {
    yield takeEvery(LIKE_MESSAGE, likeMessage);
}

export function* fetchLikedMessages(action) {
    const { id } = action.payload;
    const token = localStorage.getItem('token');
    const headerParams = { "Authorization": `Bearer ${token}` }
    try {
        const likedMessages = yield call(axios.get, `http://localhost:8080/users/${id}/like`, { headers: headerParams });
        yield put({ type: FETCH_LIKED_MESSAGES_SUCCESS, payload: { likedMessages: likedMessages.data } })
    } catch (error) {
        console.log(error);
    }
}

function* watchFetchLikedMessages() {
    yield takeEvery(FETCH_LIKED_MESSAGES, fetchLikedMessages);
}

export function* login(action) {
    console.log(action.payload)
    const { email, password } = action.payload;
    try {
        const login = yield call(axios.post, `http://localhost:8080/login`, { email, password });
        console.log(login.data)
        yield put({ type: LOGIN_SUCCESS, payload: { user: login.data.user.firstName, avatar: login.data.user.firstName, userId: login.data.user.id, token: login.data.token } })
    } catch (error) {
        console.log(error.message);
    }
}

function* watchLogin() {
    yield takeEvery(LOGIN, login);
}

export default function* chatSagas() {
    yield all([
        watchFetchMessages(),
        watchAddMessage(),
        watchEditMessage(),
        watchDeleteMessage(),
        watchLikeMessage(),
        watchFetchLikedMessages(),
        watchLogin()
    ]);
}