import { DELETE_MESSAGE, ADD_MESSAGE, EDIT_MESSAGE, LIKE_MESSAGE, FETCH_MESSAGES, FETCH_LIKED_MESSAGES, LOGIN, LOGOUT } from "./chatActionTypes"

export const deleteMessage = id => ({
    type: DELETE_MESSAGE,
    payload: {
        id
    }
});

export const addMessage = (text, date, userId) => ({
    type: ADD_MESSAGE,
    payload: {
        text,
        date,
        userId
    }
});

export const editMessage = (text, id, date) => ({
    type: EDIT_MESSAGE,
    payload: {
        text,
        id,
        date
    }
});

export const likeMessage = (id, likedMessages) => ({
    type: LIKE_MESSAGE,
    payload: {
        id,
        likedMessages
    }
});

export const fetchMessages = () => ({
    type: FETCH_MESSAGES
});

export const fetchLikedMessages = (id) => ({
    type: FETCH_LIKED_MESSAGES,
    payload: {
        id
    }
});

export const login = (email, password) => ({
    type: LOGIN,
    payload:{
        email,
        password
    }
});

export const logout = () => ({
    type: LOGOUT
});
