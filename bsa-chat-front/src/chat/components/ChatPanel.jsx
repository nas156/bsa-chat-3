import React from "react";
import MsgInput from "./MsgInput";
import Message from "./Message";
import MyMessage from "./MyMessage";
import DateBreacker from "../../common/DateBreack";
import ChatHeader from './ChatHeder';
import { formDate, formTime, compareDates } from "../../helper/DateHelpers";
import * as actions from '../chatActions';
import { connect } from "react-redux";
import { fetchMessage } from '../../edit/editActions';
import KeyboardEventHandler from 'react-keyboard-event-handler';
import { Link } from "react-router-dom";

class ChatPanel extends React.Component {
    constructor(props) {
        super(props);
        this.addMessage = this.addMessage.bind(this);
        this.scrollToBottom = this.scrollToBottom.bind(this);
        this.deleteMessage = this.deleteMessage.bind(this);
        this.editMessage = this.editMessage.bind(this);
        this.likeMessage = this.likeMessage.bind(this);
    }

    messagesEnd = React.createRef();

    scrollToBottom() {
        this.messagesEnd.current.scrollIntoView({ behavior: "smooth" });
    }

    componentDidMount() {
        this.props.fetchMessages();
        this.props.fetchLikedMessages(this.props.userId);
        window.addEventListener("keydown", function (e) {
            if ([38].indexOf(e.keyCode) > -1) {
                e.preventDefault();
            }
        }, false);
        if (!this.props.isFetching) {
            this.scrollToBottom();
        }
    }

    componentDidUpdate() {
        if (!this.props.isFetching) {
            this.scrollToBottom();
        }
    }

    addMessage(message, date) {
        if (!message) {
            return;
        }
        this.props.addMessage(message, date, this.props.userId);
    }

    deleteMessage(id) {
        this.props.deleteMessage(id);
    }

    logout() {
        this.props.logout();
    }

    editMessage(id) {
        let newMessage;
        if (!id) {
            newMessage = this.props.list.slice().reverse().find(message => message.userId === this.props.userId);
            if (newMessage) {
                id = newMessage.id;
                this.props.history.push("/edit");
            } else {
                return;
            }
        }
        this.props.fetchMessage(id);
    }

    likeMessage(id) {
        let likedMessages;
        if (this.props.likedMessages.includes(id)) {
            likedMessages = this.props.likedMessages.filter(item => item !== id);
        } else {
            likedMessages = [...this.props.likedMessages, id];
        }
        this.props.likeMessage(this.props.userId, likedMessages);
    }

    render() {
        if (this.props.isFetching) {
            return <div className="loader"></div>;
        }
        let curent_date = new Date();
        if (this.props.list.length > 0) {
            curent_date = new Date(this.props.list[0].createdAt);
        }
        let linkToUsers;
        if (this.props.user === 'admin') {
            linkToUsers = <Link to='/users' className="users-link">users</Link>;
        }
        let userId = this.props.userId;
        return (
            <div className="main-content">
                {linkToUsers}
                <Link to='/' onClick={() => this.logout()} className="logout">logout</Link>
                <ChatHeader users={this.props.users} online='10'
                    lastMessage={formTime(new Date(this.props.currentDate))} />
                <div className="chat-panel" key='chat'>
                    <DateBreacker date={formDate(curent_date)} />
                    {this.props.list.map(item => {
                        let item_date = new Date(item.createdAt);
                        let component;
                        if (item.userId === userId) {
                            component = <MyMessage
                                time={formTime(new Date(item.createdAt))}
                                message={item.text}
                                key={item.id}
                                onDelete={this.deleteMessage}
                                onEdit={this.editMessage}
                                id={item.id}
                            />;
                        } else {
                            component = <Message
                                time={formTime(new Date(item.createdAt))}
                                author={item.avatar}
                                message={item.text}
                                key={item.id}
                                id={item.id}
                                onLike={this.likeMessage}
                                liked={this.props.likedMessages.includes(item.id) ? 'liked' : ''} />;
                        }
                        if (compareDates(item_date, curent_date)) {
                            curent_date = item_date;
                            return [<DateBreacker date={formDate(item_date)} key={item_date} />, component];
                        }
                        ;
                        return component;
                    })
                    }
                    <KeyboardEventHandler
                        handleKeys={['up']}
                        onKeyEvent={() => this.editMessage('')} />
                    <MsgInput onClick={this.addMessage} />
                    <div ref={this.messagesEnd} />
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.chat.user,
        isFetching: state.chat.isFetching,
        likedMessages: state.chat.likedMessages,
        users: state.chat.users,
        list: state.chat.list,
        userId: state.chat.userId,
        currentDate: state.chat.currentDate
    }
}

const mapDispatchToProps = {
    ...actions,
    fetchMessage
}

export default connect(mapStateToProps, mapDispatchToProps)(ChatPanel);
