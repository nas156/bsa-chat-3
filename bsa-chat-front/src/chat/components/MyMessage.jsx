import React from 'react';
import { Link } from 'react-router-dom';

class MyMessage extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            text: this.props.message
        }
    }

    onChange(e) {
        const value = e.target.value;
        this.setState({
            text: value
        });
    }


    render() {
        return (
            <div className='my-msg'>
                <span className='time'>{this.props.time}</span>
                <div className='tool-bar'>
                    <button className='delete' onClick={() => this.props.onDelete(this.props.id)}>delete</button>
                    <Link to='/edit' className='edit' onClick={() => this.props.onEdit(this.props.id)}>edit</Link>
                </div>
                <span className='msg'>{this.props.message}</span>
            </div>
        )
    }
}

export default MyMessage;
