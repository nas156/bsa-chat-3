import { FETCH_USERS_SUCCESS } from "./usersActionTypes"

const initialState = {
    list: [],
    isFetching: true
}

export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_USERS_SUCCESS: {
            const { list } = action.payload;
            return ({
                list: list,
                isFetching: false
            });
        }

        default:
            return state;
    }
}