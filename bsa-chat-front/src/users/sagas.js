import { call, put, takeEvery, all } from "redux-saga/effects";
import axios from "axios";
import { FETCH_USERS_SUCCESS, FETCH_USERS, DELETE_USER } from "./usersActionTypes";

export function* fetchUsers() {
    const token = localStorage.getItem('token');
    const headerParams = { "Authorization": `Bearer ${token}` }
    try {
        const list = yield call(axios.get, 'http://localhost:8080/users/all', {headers: headerParams});
        yield put({ type: FETCH_USERS_SUCCESS, payload: { list: list.data } });
    } catch (error) {
        console.log(error.message);
    }
}

function* watchFetchUsers() {
    yield takeEvery(FETCH_USERS, fetchUsers);
}

export function* deleteUser(action) {
    const { id } = action.payload;
    const token = localStorage.getItem('token');
    const headerParams = { "Authorization": `Bearer ${token}` }
    try{
        yield call(axios.delete, `http://localhost:8080/users/${id}`, {headers: headerParams});
        yield put({type: FETCH_USERS});
    }catch(error){
        console.log(error);
    }
}

function* watchDeleteUsers(){
    yield takeEvery(DELETE_USER, deleteUser);
}

export default function* chatSagas() {
    yield all([
        watchFetchUsers(),
        watchDeleteUsers()
    ]);
}