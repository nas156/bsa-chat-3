import React from 'react';
import * as actions from './usersActions';
import { connect } from 'react-redux';
import User from './User';
import { fetchUser } from '../editUsers/editUsersActions';
import { Link } from 'react-router-dom';

class Users extends React.Component {
    constructor(props) {
        super(props);
        this.onDelete = this.onDelete.bind(this);
        this.editUser = this.editUser.bind(this);
    }

    componentDidMount() {
        if(this.props.name !== 'admin'){
            return;
        }
        this.props.fetchUsers();
    }

    editUser(id) {
        this.props.fetchUser(id);
    }

    onDelete(id) {
        console.log(id);
        this.props.deleteUser(id);
    }

    render() {
        if (this.props.isFetching) {
            return <div className="loader"></div>;
        }
        return (
            <div className="users-panel">
                {this.props.list.map(item => {
                    return <User name={item.firstName}
                        email={item.email}
                        key={item.id}
                        id={item.id}
                        onDelete={this.onDelete}
                        onEdit={this.editUser} />
                })}
                <Link to="/editUsers">add new user</Link>
                <Link to="/chat">chat</Link>
            </div>
        );
    }

}

const mapStateToProps = (state) => {
    return {
        name: state.chat.user,
        list: state.users.list,
        isFetching: state.users.isFetching
    }
}

const mapDispatchToProps = {
    ...actions,
    fetchUser
}

export default connect(mapStateToProps, mapDispatchToProps)(Users);