import React from 'react';
import { Link } from 'react-router-dom';

class User extends React.Component {
    render() {
        return (
            <div className="user">
                <div className="username-email">
                    <span>{this.props.name}</span>
                    <span> {this.props.email}</span>
                </div>
                <button className="user-delete" onClick={() => this.props.onDelete(this.props.id)}>delete</button>
                <Link to='/editUsers' className="edit-user" onClick={() => this.props.onEdit(this.props.id)}>edit</Link>
            </div>
        );
    }
}

export default User;