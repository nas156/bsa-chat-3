import {all} from 'redux-saga/effects';
import chatSagas from '../chat/sagas';
import editSagas from '../edit/sagas';
import usersSagas from '../users/sagas';
import editUsersSagas from '../editUsers/sagas';

export default function* rootSaga(){
    yield all([
        chatSagas(),
        editSagas(),
        usersSagas(),
        editUsersSagas()
    ])
}