import chat from '../chat/chatRedusers';
import edit from '../edit/editRedusers';
import users from '../users/usersResusers';
import editUsers from '../editUsers/editUsersRedusers';
import {combineReducers} from 'redux';


const rootReducer = combineReducers({chat, edit, users, editUsers});

export default rootReducer;
