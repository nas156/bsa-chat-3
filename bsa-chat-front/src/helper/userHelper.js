export function getDefaultUserData() {
    return ({
        firstName: '',
        lastName: '',
        password: '',
        avatar: '',
        id: '',
        email: ''
    });
}