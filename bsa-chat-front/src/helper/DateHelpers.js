export function formDate(curent_date) {
    const month = curent_date.getMonth() + 1;
    const formatMonth = (month > 9) ? month : ('0' + month)
    const date = curent_date.getDate();
    const formatDate = (date > 9) ? date : ('0' + date);
    return curent_date.getFullYear() + '-' + formatMonth + '-' + formatDate;
}

export function formTime(date) {
    const hours = date.getHours();
    const formatHours = (hours > 9) ? hours : ('0' + hours)
    const minutes = date.getMinutes();
    const formatMinutes = (minutes > 9) ? minutes : ('0' + minutes);
    return formatHours + ':' + formatMinutes;
}

export function compareDates(date1, date2) {
    return (date1.getFullYear() > date2.getFullYear() ||
        date1.getMonth() > date2.getMonth() ||
        date1.getDate() > date2.getDate());
}
