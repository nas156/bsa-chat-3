import { login } from '../chat/chatActions';
import React from 'react';
import { Form, Button, Segment } from 'semantic-ui-react';
import { connect } from 'react-redux';
import validator from 'validator';


class LoginForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      email: '',
      password: '',
      isPasswordValid: true,
      isEmailValid: true,
    }
  }

  componentDidMount(){
    if (this.props.isLogined) {
      this.props.history.push("/chat");
    }
  }

  componentDidUpdate(){
    if (this.props.isLogined) {
      this.props.history.push("/chat");
    }
  }

  emailChanged = data => {
    this.setState({
      ...this.state,
      email: data,
      isEmailValid: true
    });
  };

  passwordChanged = data => {
    this.setState({
      ...this.state,
      password: data,
      isPasswordValid: true
    });
  };

  handleLoginClick = () => {
    const isValid = this.state.isEmailValid && this.state.isPasswordValid;
    if (!isValid) {
      return;
    }

    this.props.login(this.state.email, this.state.password);
    
  }


  render() {
    return (
      <Form name="loginForm" size="large" onSubmit={this.handleLoginClick} className="loginForm">
        <Segment>
          <Form.Input
            fluid
            icon="at"
            iconPosition="left"
            placeholder="Email"
            type="email"
            error={!this.state.isEmailValid}
            onChange={ev => this.emailChanged(ev.target.value)}
            onBlur={() => this.setState({
              ...this.state,
              isEmailValid: validator.isEmail(this.state.email)
            })}
          />
          <Form.Input
            fluid
            icon="lock"
            iconPosition="left"
            placeholder="Password"
            type="password"
            error={!this.state.isPasswordValid}
            onChange={ev => this.passwordChanged(ev.target.value)}
            onBlur={() => this.setState({
              ...this.state,
              isPasswordValid: Boolean(this.state.password)
            })}
          />
          <Button type="submit" color="teal" fluid size="large" primary>
            Login
        </Button>
        </Segment>
      </Form>
    );
  };
}

const mapStateToProps = (state) => {
  return {
    isLogined: state.chat.isLogined
  }
}

const mapDispatchToProps = {
  login
}


export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);
